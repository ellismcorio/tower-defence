using System;

namespace TreehouseDefense
{
    class FastInvader : Invader
    {
        public override double Health { get; protected set; } = 2.0;
        protected override int StepSize { get; } =2;
        public FastInvader(Path path) : base(path)
        {
            
        }
    }
}
