using System;

namespace TreehouseDefense
{
    class ShotgunTower : Tower
    {
        protected override double Power { get; }  = 4.0;
        protected override double Accuracy { get; }  = 0.5;

        public ShotgunTower(MapLocation location) :base(location)
        {
    
        }
    }
}