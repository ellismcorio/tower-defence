using System;

namespace TreehouseDefense
{
    class LaserTower : Tower
    {
        protected override double Power { get; }  = 0.5;
        protected override double Accuracy { get; }  = 1.0;
        public LaserTower(MapLocation location) :base(location)
        {
    
        }
    }
}