namespace TreehouseDefense
{
    class ShieldedInvader : Invader
    {
        public override double Health { get; protected set; } = 2.0;
        public ShieldedInvader(Path path) : base(path)
        {

        }
          public override void DecreaseHealth(double factor)
        {
            if(Random.NextDouble() < .5)
            {
                base.DecreaseHealth(factor);
            }
            else
            {
                System.Console.WriteLine("Shot at a shielded invader but it sustained no damage");
            }
        }
    }
}