using System;

namespace TreehouseDefense
{
    class BasicInvader : Invader
    {

        public override double Health { get; protected set; } = 2.0;

        public BasicInvader(Path path) : base(path)
        {
            
        }
    }
}
