using System;

namespace TreehouseDefense
{
    class StrongInvader : Invader
    {
        public override double Health { get; protected set; } = 4.0;
        public StrongInvader(Path path) : base(path)
        {
            
        }
    }
}
